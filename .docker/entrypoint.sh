#!/bin/bash

# echo 'Copy node_modules'
# cp -R /usr/src/cache/node_modules /usr/src/home-market-api
echo 'Run migrations'
yarn typeorm migration:run
echo 'Run server'
yarn start:dev