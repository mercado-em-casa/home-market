import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as doetenv from 'dotenv';
import path from 'path';

doetenv.config();

export const DB_CONNECTION = {
  type: process.env.TYPEORM_CONNECTION || 'postgres',
  url: process.env.TYPEORM_URL,
  logging: process.env.TYPEORM_LOGGING,
  synchronize: false,
  dropSchema: false,
  autoLoadEntities: true,
  migrations: [path.resolve(__dirname, '..', 'db', 'migrations', '*.{ts,js}')],
  cli: { migrationsDir: path.resolve(__dirname, '..', 'db', 'migrations') },
} as TypeOrmModuleOptions;

export const UNIT_TEST_DB_CONNECTION = {
  type: 'sqlite',
  database: path.resolve(__dirname, '..', '..', 'test', 'data', 'test.db'),
  logging: true,
  synchronize: true,
  dropSchema: true,
  autoLoadEntities: true,
  migrations: [path.resolve(__dirname, '..', 'db', 'migrations', '*.{ts,js}')],
} as TypeOrmModuleOptions;
