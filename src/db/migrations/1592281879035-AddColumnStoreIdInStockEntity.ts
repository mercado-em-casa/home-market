import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export class AddColumnStoreIdInStockEntity1592281879035
  implements MigrationInterface {
  private readonly column = new TableColumn({
    name: 'store_id',
    type: 'uuid',
    isNullable: false,
  });

  private readonly foreignKey = new TableForeignKey({
    columnNames: ['store_id'],
    referencedColumnNames: ['id'],
    referencedTableName: 'stores',
    onUpdate: 'cascade',
    onDelete: 'cascade',
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('stocks', this.column);
    await queryRunner.createForeignKey('stocks', this.foreignKey);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('stocks', this.column);
  }
}
