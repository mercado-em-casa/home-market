import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export class AddColumnStockIdInStockCategoryEntity1592281050547
  implements MigrationInterface {
  private readonly column = new TableColumn({
    name: 'stock_id',
    type: 'uuid',
    isNullable: false,
  });

  private readonly foreignKey = new TableForeignKey({
    columnNames: ['stock_id'],
    referencedColumnNames: ['id'],
    referencedTableName: 'stocks',
    onDelete: 'cascade',
    onUpdate: 'cascade',
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('QUE MERDA 2');
    await queryRunner.addColumn('stock_categories', this.column);
    await queryRunner.createForeignKey('stock_categories', this.foreignKey);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('stock_categories', this.column);
  }
}
