import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateAddress1592121477641 implements MigrationInterface {
  private readonly table = new Table({
    name: 'addresses',
    columns: [
      {
        name: 'id',
        type: 'uuid',
        isPrimary: true,
        isUnique: true,
        isGenerated: true,
        generationStrategy: 'uuid',
      },
      {
        name: 'country',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'state',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'city',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'first_line',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'second_line',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'postal_code',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table);
  }
}
