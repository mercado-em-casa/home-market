import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePerson1590451467331 implements MigrationInterface {
  private personTable = new Table({
    name: 'persons',
    columns: [
      {
        name: 'id',
        type: 'uuid',
        isPrimary: true,
        isUnique: true,
        isGenerated: true,
        generationStrategy: 'uuid',
      },
      {
        name: 'name',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'surname',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'email',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'password',
        type: 'varchar',
        isNullable: false,
      },
      {
        name: 'telephones',
        type: 'varchar',
        isNullable: false,
        isArray: true,
      },
      {
        name: 'roles',
        type: 'varchar',
        isNullable: false,
        isArray: true,
      },
      {
        name: 'is_enabled',
        type: 'boolean',
        isNullable: false,
        default: true,
      },
      {
        name: 'is_account_non_expired',
        type: 'boolean',
        isNullable: false,
        default: true,
      },
      {
        name: 'is_account_non_locked',
        type: 'boolean',
        isNullable: false,
        default: true,
      },
      {
        name: 'is_credentials_non_expired',
        type: 'boolean',
        isNullable: false,
        default: true,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.personTable);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.personTable);
  }
}
