import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateProduct1592192774769 implements MigrationInterface {
  private table = new Table({
    name: 'products',
    columns: [
      {
        name: 'id',
        type: 'uuid',
        isPrimary: true,
        isUnique: true,
        isGenerated: true,
        generationStrategy: 'uuid',
      },
      {
        name: 'description',
        type: 'text',
        isNullable: true,
      },
      {
        name: 'weight',
        type: 'numeric',
        isNullable: false,
      },
      {
        name: 'image',
        type: 'varchar',
        isNullable: true,
      },
      {
        name: 'manufacturing_date',
        type: 'date',
        isNullable: false,
      },
      {
        name: 'expiration_date',
        type: 'date',
        isNullable: false,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table);
  }
}
