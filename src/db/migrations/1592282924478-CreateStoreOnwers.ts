import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateStoreOnwers1592282924478 implements MigrationInterface {
  private readonly table = new Table({
    name: 'store_owners_stores',
    columns: [
      {
        name: 'store_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
      {
        name: 'store_owner_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
    foreignKeys: [
      {
        columnNames: ['store_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'stores',
        onUpdate: 'cascade',
      },
      {
        columnNames: ['store_owner_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'store_owners',
        onUpdate: 'cascade',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table);
  }
}
