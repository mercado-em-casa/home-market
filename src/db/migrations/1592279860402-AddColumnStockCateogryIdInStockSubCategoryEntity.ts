import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export class AddColumnStockCateogryIdInStockSubCategoryEntity1592279860402
  implements MigrationInterface {
  private readonly column = new TableColumn({
    name: 'stock_category_id',
    type: 'uuid',
    isNullable: false,
  });

  private readonly foreignKey = new TableForeignKey({
    columnNames: ['stock_category_id'],
    referencedColumnNames: ['id'],
    referencedTableName: 'stock_categories',
    onDelete: 'cascade',
    onUpdate: 'cascade',
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('stock_sub_categories', this.column);
    await queryRunner.createForeignKey('stock_sub_categories', this.foreignKey);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('stock_sub_categories', this.column);
  }
}
