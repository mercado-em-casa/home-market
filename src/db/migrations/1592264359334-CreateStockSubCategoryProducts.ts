import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateStockSubCategoryProducts1592264359334
  implements MigrationInterface {
  private readonly table = new Table({
    name: 'stock_sub_category_products',
    columns: [
      {
        name: 'stock_sub_category_id',
        type: 'uuid',
        isPrimary: true,
        isNullable: false,
      },
      {
        name: 'product_id',
        type: 'uuid',
        isPrimary: true,
        isNullable: false,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
    foreignKeys: [
      {
        columnNames: ['stock_sub_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'stock_sub_categories',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      {
        columnNames: ['product_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'products',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table);
  }
}
