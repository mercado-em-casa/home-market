import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateStoreAddresses1592282722842 implements MigrationInterface {
  private readonly table = new Table({
    name: 'store_addresses',
    columns: [
      {
        name: 'store_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
      {
        name: 'address_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
    foreignKeys: [
      {
        columnNames: ['store_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'stores',
        onUpdate: 'cascade',
      },
      {
        columnNames: ['address_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'addresses',
        onUpdate: 'cascade',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table, true);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table, true);
  }
}
