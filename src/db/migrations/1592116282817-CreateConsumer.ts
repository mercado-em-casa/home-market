import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateConsumer1592116282817 implements MigrationInterface {
  private readonly table = new Table({
    name: 'consumers',
    columns: [
      {
        name: 'id',
        type: 'uuid',
        isPrimary: true,
        isUnique: true,
        isGenerated: true,
        generationStrategy: 'uuid',
      },
      {
        name: 'person_id',
        type: 'uuid',
        isPrimary: true,
        isUnique: true,
      },
      {
        name: 'created_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
      {
        name: 'updated_at',
        type: 'timestamp with time zone',
        default: 'CURRENT_TIMESTAMP',
      },
    ],
    foreignKeys: [
      {
        columnNames: ['person_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'persons',
        onDelete: 'cascade',
        onUpdate: 'cascade',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.table);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.table);
  }
}
