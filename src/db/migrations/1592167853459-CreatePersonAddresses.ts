import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePersonAddresses1592167853459 implements MigrationInterface {
  private readonly personAddressesTable = new Table({
    name: 'person_addresses',
    columns: [
      {
        name: 'person_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
      {
        name: 'address_id',
        type: 'uuid',
        isNullable: false,
        isPrimary: true,
      },
    ],
    foreignKeys: [
      {
        columnNames: ['person_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'persons',
        onUpdate: 'cascade',
      },
      {
        columnNames: ['address_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'addresses',
        onUpdate: 'cascade',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.personAddressesTable);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.personAddressesTable);
  }
}
