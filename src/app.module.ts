import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { environmentsSchemaValidation } from './core/validations/environments-schema-validation.utils';
import { DB_CONNECTION } from 'config/orm';
import { PersonsModule } from './persons/person.module';
import { AddressModule } from './address/address.module';
import { DeliveryManModule } from './delivery-man/delivery-man.module';
import { ConsumerModule } from './consumer/consumer.module';
import { StoreModule } from './store/store.module';
import { StockModule } from './stock/stock.module';
import { StockCategoryModule } from './stock-category/stock-category.module';
import { StockSubCategoryModule } from './stock-sub-category/stock-sub-category.module';
import { ProductModule } from './product/product.module';
import { StoreOwnerModule } from './store-owner/store-owner.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: environmentsSchemaValidation(),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: () => DB_CONNECTION,
      inject: [ConfigService],
    }),
    PersonsModule,
    AddressModule,
    DeliveryManModule,
    ConsumerModule,
    StoreModule,
    StockModule,
    StockCategoryModule,
    StockSubCategoryModule,
    ProductModule,
    StoreOwnerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
