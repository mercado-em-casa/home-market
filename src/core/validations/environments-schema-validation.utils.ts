import * as Joi from '@hapi/joi';

export function environmentsSchemaValidation(): Joi.ObjectSchema {
  return Joi.object({
    NODE_ENV: Joi.string().valid(
      'development',
      'production',
      'test',
      'provision',
    ),
    PORT: Joi.number(),
  });
}
