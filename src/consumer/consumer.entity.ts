import {
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  OneToOne,
  CreateDateColumn,
} from 'typeorm';
import { Person } from 'persons/person.entity';

@Entity({ name: 'consumers' })
export class Consumer {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @JoinColumn({ name: 'person_id', referencedColumnName: 'id' })
  @OneToOne(() => Person, {
    eager: true,
    cascade: ['insert', 'update'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    nullable: false,
    primary: true,
  })
  profile: Person;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @CreateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
