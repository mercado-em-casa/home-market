import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreOwner } from './store-owner.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StoreOwner])],
})
export class StoreOwnerModule {}
