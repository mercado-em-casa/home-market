import {
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Person } from 'persons/person.entity';

@Entity({ name: 'delivery_mans' })
export class DeliveryMan {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @JoinColumn({ name: 'person_id', referencedColumnName: 'id' })
  @OneToOne(() => Person, {
    eager: true,
    cascade: ['insert', 'update'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    nullable: false,
    primary: true,
  })
  profile: Person;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
