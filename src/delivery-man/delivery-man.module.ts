import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DeliveryMan } from './delivery-man';

@Module({
  imports: [TypeOrmModule.forFeature([DeliveryMan])],
})
export class DeliveryManModule {}
