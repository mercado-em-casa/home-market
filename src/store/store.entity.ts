import { Address } from 'address/address.entity';
import { Stock } from 'stock/stock.entity';
import { StoreOwner } from 'store-owner/store-owner.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'stores' })
export class Store {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column('varchar', { nullable: false, array: true })
  telephones: string[];
  @Column({ nullable: false })
  openingTime: string;
  @Column({ nullable: false })
  closingTime: string;
  @OneToMany(
    () => Stock,
    stock => stock.store,
    { nullable: true, cascade: ['insert', 'update'] },
  )
  stocks: Stock[];
  @ManyToMany(() => Address, {
    cascade: ['insert', 'update'],
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'store_addresses',
    joinColumn: { name: 'store_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'address_id', referencedColumnName: 'id' },
  })
  addresses: Address[];
  @ManyToMany(() => StoreOwner, {
    cascade: ['insert', 'update'],
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'store_owners_stores',
    joinColumn: { name: 'store_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'store_owner_id', referencedColumnName: 'id' },
  })
  onwers: StoreOwner[];
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
