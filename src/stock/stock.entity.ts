import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StockCategory } from 'stock-category/stock-category.entity';
import { Store } from 'store/store.entity';

@Entity({ name: 'stocks' })
export class Stock {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: true })
  description: string;
  @OneToMany(
    () => StockCategory,
    stockCategory => stockCategory.stock,
    { nullable: true, cascade: ['insert', 'update'] },
  )
  stockCategories: StockCategory[];
  @ManyToOne(
    () => Store,
    store => store.stocks,
    {
      nullable: false,
      cascade: ['insert', 'update'],
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'store_id', referencedColumnName: 'id' })
  store: Store;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
