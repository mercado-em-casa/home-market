import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as dotenv from 'dotenv';
import Helmet from 'helmet';
import { AppModule } from 'app.module';

async function bootstrap() {
  dotenv.config();
  const logger = new Logger('HomeMarketApi');
  const port = parseInt(process.env.PORT, 10) || 3000;
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true }),
  );
  app.setGlobalPrefix('api');
  app.use(Helmet());
  app.enableCors({
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Authorization'],
  });
  await app.listen(port, '0.0.0.0', () => {
    logger.log(`Server is running on port ${port}`);
  });
}
bootstrap();
