import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from 'product/product.entity';
import { StockCategory } from 'stock-category/stock-category.entity';

@Entity({ name: 'stock_sub_categories' })
export class StockSubCategory {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: true })
  description?: string;
  @ManyToOne(
    () => StockCategory,
    stockCategory => stockCategory.stockSubCategories,
    {
      eager: true,
      nullable: false,
      cascade: ['insert', 'update'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'stock_category_id', referencedColumnName: 'id' })
  stockCategory: StockCategory;
  @ManyToMany(() => Product, {
    cascade: ['insert', 'update'],
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinTable({
    name: 'stock_sub_category_products',
    joinColumn: { name: 'stock_sub_category_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'product_id', referencedColumnName: 'id' },
  })
  products: Product[];
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
