import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockSubCategory } from './stock-sub-category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockSubCategory])],
})
export class StockSubCategoryModule {}
