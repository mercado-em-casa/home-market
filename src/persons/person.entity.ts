import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Address } from 'address/address.entity';

@Entity({ name: 'persons' })
export class Person {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: false })
  surname: string;
  @Column({ nullable: false, unique: true })
  email: string;
  @Column({ nullable: false })
  password: string;
  @Column('varchar', { nullable: false, array: true })
  telephones: string[];
  @ManyToMany(() => Address, {
    cascade: ['insert', 'update'],
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'person_addresses',
    joinColumn: { name: 'person_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'address_id', referencedColumnName: 'id' },
  })
  addresses: Address[];
  @Column('varchar', { nullable: false, array: true })
  roles: string[];
  @Column({ name: 'is_enabled', default: true })
  isEnabled?: boolean;
  @Column({ name: 'is_account_non_expired', default: true })
  isAccountNonExpired?: boolean;
  @Column({ name: 'is_account_non_locked', default: true })
  isAccountNonLocked?: boolean;
  @Column({ name: 'is_credentials_non_expired', default: true })
  isCredentialsNonExpired?: boolean;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
