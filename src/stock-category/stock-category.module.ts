import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockCategory } from './stock-category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockCategory])],
})
export class StockCategoryModule {}
