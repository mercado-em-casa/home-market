import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StockSubCategory } from 'stock-sub-category/stock-sub-category.entity';
import { Stock } from 'stock/stock.entity';

@Entity({ name: 'stock_categories' })
export class StockCategory {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: true })
  description?: string;
  @OneToMany(
    () => StockSubCategory,
    stockSubCategory => stockSubCategory.stockCategory,
    { nullable: true, cascade: ['insert', 'update'] },
  )
  stockSubCategories: StockSubCategory[];
  @ManyToOne(
    () => Stock,
    stock => stock.stockCategories,
    {
      eager: true,
      nullable: false,
      cascade: ['insert', 'update'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'stock_id', referencedColumnName: 'id' })
  stock: Stock;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
