import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity({ name: 'addresses' })
export class Address {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  country: string;
  @Column({ nullable: false })
  state: string;
  @Column({ nullable: false })
  city: string;
  @Column({ name: 'first_line', nullable: false })
  firstLine: string;
  @Column({ name: 'second_line' })
  secondLine: string;
  @Column({ name: 'postal_code', nullable: false })
  postalCode: string;
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @CreateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
