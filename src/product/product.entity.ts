import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'products' })
export class Product {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: true })
  description?: string;
  @Column({ type: 'numeric', nullable: false })
  weight: number;
  @Column({ nullable: true })
  image?: string;
  @Column({ name: 'manufacturing_date', type: 'date', nullable: false })
  manufacturingDate: Date;
  @Column({ name: 'expiration_date', type: 'date', nullable: false })
  expirationDate: Date;
  @Column({ nullable: false })
  brand: string; //TODO: criar nova entidade para brand
  @CreateDateColumn({ name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;
  @UpdateDateColumn({ name: 'updated_at', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt?: Date;
}
